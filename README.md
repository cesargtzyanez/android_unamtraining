# Basic Android Training For UNAM #

This repo has some projects for Basic Android Training, so all you need to run is the Android Studio.

### What are the topics? ###

Every topic is in a different branch, but accidentally I added every previous Project in the new branch, so the last branch has all the projects.

* Project: **Devices**. Branch: **project00_Devices**  simple project just to get familiar with Android Studio

* Project: **BasicControl** Branch: **project01_BasicControlApp** : In this project, the student knows the very basic controls some of their parameters and properties

* Project: **ViewsExample** Branch: **project02_ViewsExample**: This project is about knowing the use of the Views components.

* Project **BasicControls**. Branch: **project03_BasicControls**: This project is for go deeper into the understanding of some controls.

* Project: **ListsExample**. Branch: **project04_Lists**: This project is an example of the List component, using also adapters for display the list items.

* Project: **WebViewExample**. Branch: **project05_WebView**: This example uses WebChromeClient 

* Project: **ContactsList**.Branch: **project06_PersonalProject**: The Personal Project is a final project wich goal is use all the knowledge acquired. The app goal is to create simple Contacts List, so the users can add, edit, and remove them from the database.

* Project: **BasicControls** Branch: **project07_Intent**: In this lesson the student sends som info from the Main Activity to another Activity.

* Project: **BasicControls** Branch: **project08_BDExample**: The goal of this project is to manage the local SQLite database for android.

* Project: **BasicControls** Branch: **project09_SharedPreferences**: For this project, the student will learn about SharedPreferences.

* Project: **BasicControls** Branch: **project10_Widget**: This project is not complete, but the goal is to create a widget for the BasicControls App.

The original source of these projects is this [repo](https://bitbucket.org/tazmadroid/androidbasico)